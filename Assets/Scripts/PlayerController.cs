using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance { get; private set; }
    Rigidbody rigidbody;

    private void Awake()
    {
        if(Instance != null)
        {
            Debug.LogError("Only one instantion of PlayerController can exist!");
            return;
        }
        Instance = this;

        rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Shader.SetGlobalVector("_PlayerPosition", PlayerController.Instance.transform.position);
    }

    private void FixedUpdate()
    {
        var movmenetInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        var goalVelocity = movmenetInput * 5f;
        rigidbody.velocity = new Vector3(goalVelocity.x, rigidbody.velocity.y, goalVelocity.y);
    }
}
