﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkinController : MonoBehaviour
{
    [SerializeField] Animator animator;
    Rigidbody playerRigidbody;
    Vector3 lastUpdatePosition;

    private void Awake()
    {
        playerRigidbody = GetComponentInParent<Rigidbody>();
    }

    private void Reset()
    {
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        var velocity = playerRigidbody.velocity.normalized;
        velocity = transform.InverseTransformVector(velocity);
        animator.SetFloat("MovementX", velocity.x);
        animator.SetFloat("MovementY", velocity.z);

        //To nie powinno tu istnieć, ale dla testów
        if(Input.GetKeyDown(KeyCode.Space))
        {
            animator.SetTrigger("UseFireball");
        }
    }

    private void LateUpdate()
    {
        lastUpdatePosition = transform.position;
    }
}
